// Lấy thư viện hệ thống
import dotenv from "dotenv"
import cors from "cors"
import express from "express"
import morgan from "morgan"

// Lấy thư viện tự tạo
import RouterCauHoi from "./routes/cauhoi"
import RouterCauHoiDeThi from "./routes/cauhoidethi"
import RouterCauTraLoi from "./routes/cautraloi"
import RouterDeThi from "./routes/dethi"
import RouterDeThiMonHoc from "./routes/dethimonhoc"
import RouterLopHoc from "./routes/lophoc"
import RouterNguoiDung from "./routes/nguoidung"
import RouterQuyen from "./routes/quyen"
import RouterChuong from "./routes/chuong"
import RouterMonhoc from "./routes/monhoc"
// Setup các biến
dotenv.config()
const ADDRESS: string = process.env.API_ADDRESS || "localhost"
const PORT: number = parseInt(process.env.API_PORT as string, 10) || 3000
const app = express()

// Sử dụng các thư viện
app.use(express.json())
app.use(cors())
app.use(morgan("tiny"))

// Gom các routes
app.use("/api/cauhoi", RouterCauHoi)
app.use("/api/cautraloi", RouterCauTraLoi)
app.use("/api/dethi", RouterDeThi)
app.use("/api/dethimonhoc", RouterDeThiMonHoc)
app.use("/api/cauhoidethi", RouterCauHoiDeThi)
app.use("/api/lophoc", RouterLopHoc)
app.use("/api/nguoidung", RouterNguoiDung)
app.use("/api/quyen", RouterQuyen)
app.use("/api/chuong", RouterChuong)
app.use("api/monhoc", RouterMonhoc)

// Khởi động server
app.listen(PORT, () => {
  console.log(`Server is running at http://${ADDRESS}:${PORT}`)
})