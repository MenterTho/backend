import { Request, Response } from "express"
import ServiceChuong from "../services/chuong"
import HttpStatus from "http-status-codes"
import { ArrayChuong, OnlyChuong } from "../models/chuong"
import { LastOrCount } from "../models/alltypes"

async function insert(req: Request, res: Response) {
  try {
    const chuong_number: number = req.body.monhoc_id
    const name: string = req.body.name;
    if (isNaN(chuong_number) || chuong_number < 1) {
      res
        .status(HttpStatus.BAD_REQUEST)
        .json({ message: "Bad Request: Id are required." })
      return
    }

    const service: ServiceChuong = new ServiceChuong()
    service.chuong_name = name
    service.chuong_number = chuong_number
    const result: boolean = await service.insert()
    if (result) {
      res
        .status(HttpStatus.OK)
        .json({ message: "Insert CauHoi successfully!" })
    } else {
      res
        .status(HttpStatus.INTERNAL_SERVER_ERROR)
        .json({ message: "Internal Server Error: Unable to fetch data." })
    }
  } catch (error) {
    console.error("Error in ControllerCauHoi:", error)
    res
      .status(HttpStatus.INTERNAL_SERVER_ERROR)
      .json({ message: "Internal Server Error" })
  }
}
//

//
async function update(req: Request, res: Response): Promise<void> {
  try {
    const id: number = req.body.id
    const monhoc_id: number = req.body.monhoc_id
    const number: number = req.body.chuong_number
    const name: string = req.body.chuong_name

    if (
      isNaN(monhoc_id) ||
      isNaN(id) ||
      isNaN(number) ||
      monhoc_id < 1 ||
      id < 1 ||
      number < 1
    ) {
      res.status(HttpStatus.BAD_REQUEST).json({
        message:
          "Bad Request: Id is required and chuong_id, cautraloi_id are optional.",
      });
      return
    }
    const service: ServiceChuong = new ServiceChuong()
    service.chuong_id = id
    service.monhoc_id = monhoc_id
    service.chuong_number = number
    service.chuong_name = name
    const result: boolean = await service.update()
    if (result) {
      res
        .status(HttpStatus.OK)
        .json({ message: "Update LopHoc successfully!" })
    } else {
      res
        .status(HttpStatus.INTERNAL_SERVER_ERROR)
        .json({ message: "Internal Server Error: Unable to fetch data." })
    }
  } catch (error) {
    console.error("Error in ControllerLopHoc:", error)
    res
      .status(HttpStatus.INTERNAL_SERVER_ERROR)
      .json({ message: "Internal Server Error" })
  }
}
//
async function remove(req: Request, res: Response): Promise<void> {
  try {
    const id: number = parseInt(req.params.id as string, 10)

    console.log(id)

    if (isNaN(id) || id < 1) {
      res
        .status(HttpStatus.BAD_REQUEST)
        .json({ message: "Bad Request: id is required." })
      return;
    }

    const service: ServiceChuong = new ServiceChuong()
    service.chuong_id = id
    const result: boolean = await service.remove()

    if (result) {
      res
        .status(HttpStatus.OK)
        .json({ message: "Remove LopHoc successfully!" })
    } else {
      res
        .status(HttpStatus.INTERNAL_SERVER_ERROR)
        .json({ message: "Internal Server Error: Unable to fetch data." })
    }
  } catch (error) {
    console.error("Error in ControllerLopHoc:", error)
    res
      .status(HttpStatus.INTERNAL_SERVER_ERROR)
      .json({ message: "Internal Server Error" })
  }
}

async function get(req: Request, res: Response): Promise<void> {
  try {
    const id: number = parseInt(req.params.id, 10)

    if (isNaN(id) || id < 1) {
      res
        .status(HttpStatus.BAD_REQUEST)
        .json({ message: "Bad Request: Id are required." })
      return
    }

    const service: ServiceChuong = new ServiceChuong()
    service.chuong_id = id;
    const result: OnlyChuong = await service.get()

    if (result) {
      res.status(HttpStatus.OK).json(result)
    } else {
      res
        .status(HttpStatus.INTERNAL_SERVER_ERROR)
        .json({ message: "Internal Server Error: Unable to fetch data." })
    }
  } catch (error) {
    console.error("Error in ControllerChuong:", error)
    res
      .status(HttpStatus.INTERNAL_SERVER_ERROR)
      .json({ message: "Internal Server Error" })
  }
}
async function getAllChuongOnMonHoc(
  req: Request,
  res: Response
): Promise<void> {
  try {
    const chuong_id: number = parseInt(req.params.id, 10)
    const limit: number = parseInt(req.query.limit as string, 10)
    const offset: number = parseInt(req.query.offset as string, 10)

    if (
      isNaN(chuong_id) ||
      isNaN(limit) ||
      isNaN(offset) ||
      chuong_id < 1 ||
      limit < 1 ||
      offset < 0
    ) {
      res.status(HttpStatus.BAD_REQUEST).json({
        message:
          "Bad Request: Both chuong_id params, limit and offset query are required.",
      })
      return
    }

    const service: ServiceChuong = new ServiceChuong()
    const result: ArrayChuong = await service.getAllChuongOnMonHoc(
      chuong_id,
      limit,
      offset
    );

    if (result) {
      res.status(HttpStatus.OK).json(result)
    } else {
      res
        .status(HttpStatus.INTERNAL_SERVER_ERROR)
        .json({ message: "Internal Server Error: Unable to fetch data." })
    }
  } catch (error) {
    console.error("Error in ControllerChuong:", error)
    res
      .status(HttpStatus.INTERNAL_SERVER_ERROR)
      .json({ message: "Internal Server Error" })
  }
}
async function getAll(req: Request, res: Response) {
  try {
    const { limit, offset } = req.query;
    if (!limit || !offset) {
      res
        .status(HttpStatus.BAD_REQUEST)
        .json({ message: "Bad Request: Both limit and offset are required." })
      return;
    }
    const service = new ServiceChuong();
    const result = await service.getAll(
      parseInt(limit as string),
      parseInt(offset as string)
    );
    if (result) {
      res.json(result);
    } else {
      res
        .status(500)
        .json({ message: "Internal Server Error: Unable to fetch data." })
    }
  } catch (error) {
    console.error("Error in ControllerChuong:", error)
    res.status(500).json({ message: "Internal Server Error" })
  }
}

async function getCount(res: Response): Promise<void> {
  try {
    const service: ServiceChuong = new ServiceChuong()
    const result: LastOrCount = await service.getCount()

    if (result) {
      res.status(HttpStatus.OK).json(result)
    } else {
      res
        .status(HttpStatus.INTERNAL_SERVER_ERROR)
        .json({ message: "Internal Server Error: Unable to fetch data." })
    }
  } catch (error) {
    console.error("Error in ControllerChuong:", error)
    res
      .status(HttpStatus.INTERNAL_SERVER_ERROR)
      .json({ message: "Internal Server Error" })
  }

}
async function searchAll(req: Request, res: Response): Promise<void> {
  try {
    const keyword: string = req.params.keyword
    const limit: number = parseInt(req.query.limit as string)
    const offset: number = parseInt(req.query.offset as string)

    if (!keyword || isNaN(limit) || isNaN(offset) || limit < 1 || offset < 0) {
      res.status(HttpStatus.BAD_REQUEST).json({ message: "Bad Request: Both keyword, keyword, limit and offset are required." })
      return
    }

    const service: ServiceChuong = new ServiceChuong()
    const result: ArrayChuong = await service.searchAll(keyword, limit, offset)

    if (result) {
      res.status(HttpStatus.OK).json(result)
    } else {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error: Unable to fetch data." })
    }
  } catch (error) {
    console.error("Error in ControllerChuong:", error)
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error" })
  }
}
export default {
  insert,
  getAll,
  get,
  remove,
  update,
  getCount,
  getAllChuongOnMonHoc,searchAll

}
