import { Request, Response } from "express"
import HttpStatus from "http-status-codes"

import ServiceMonhoc from "../services/monhoc"
import { OnlyMonHoc, ArrayMonHoc } from "../models/monhoc"

async function insert(req: Request, res: Response): Promise<void> {
  try {
    const name: string = req.body.name;

    if (!name) {
      res
        .status(HttpStatus.BAD_REQUEST)
        .json({ message: "Bad Request: name is required." })
      return
    }

    const service: ServiceMonhoc = new ServiceMonhoc()
    service.monhoc_name = name
    const result: boolean = await service.insert()

    if (result) {
      res
        .status(HttpStatus.OK)
        .json({ message: "Insert Monhoc successfully!" })
    } else {
      res
        .status(HttpStatus.INTERNAL_SERVER_ERROR)
        .json({ message: "Internal Server Error: Unable to fetch data." })
    }
  } catch (error) {
    console.error("Error in ControllerMonHoc:", error)
    res
      .status(HttpStatus.INTERNAL_SERVER_ERROR)
      .json({ message: "Internal Server Error" })
  }
}
async function update(req: Request, res: Response): Promise<void> {
  try {
    const id: number = req.body.id
    const lophoc_id: number = req.body.lophoc_id
    const name: string = req.body.name

    if (isNaN(id) || id < 1 || !name) {
      res.status(HttpStatus.BAD_REQUEST).json({
        message:
          "Bad Request: id and name are required and chuong_id, cautraloi_id are optional.",
      })
      return
    }

    const service: ServiceMonhoc = new ServiceMonhoc()
    service.monhoc_id = id
    service.lophoc_id = lophoc_id
    service.monhoc_name = name
    const result: boolean = await service.update()

    if (result) {
      res
        .status(HttpStatus.OK)
        .json({ message: "Update Monhoc successfully!" })
    } else {
      res
        .status(HttpStatus.INTERNAL_SERVER_ERROR)
        .json({ message: "Internal Server Error: Unable to fetch data." })
    }
  } catch (error) {
    console.error("Error in ControllerMonhoc:", error)
    res
      .status(HttpStatus.INTERNAL_SERVER_ERROR)
      .json({ message: "Internal Server Error" })
  }
}
//
async function remove(req: Request, res: Response): Promise<void> {
  try {
    const id: number = parseInt(req.params.id as string, 10)
    if (isNaN(id) || id < 1) {
      res
        .status(HttpStatus.BAD_REQUEST)
        .json({ message: "Bad Request: id is required." })
      return;
    }

    const service: ServiceMonhoc = new ServiceMonhoc()
    service.monhoc_id = id
    const result: boolean = await service.remove()

    if (result) {
      res
        .status(HttpStatus.OK)
        .json({ message: "Remove Monhoc successfully!" })
    } else {
      res
        .status(HttpStatus.INTERNAL_SERVER_ERROR)
        .json({ message: "Internal Server Error: Unable to fetch data." })
    }
  } catch (error) {
    console.error("Error in ControllerMonhoc:", error)
    res
      .status(HttpStatus.INTERNAL_SERVER_ERROR)
      .json({ message: "Internal Server Error" })
  }
}
async function get(req: Request, res: Response): Promise<void> {
  try {
    const id: number = parseInt(req.params.id, 10)

    if (isNaN(id) || id < 1) {
      res
        .status(HttpStatus.BAD_REQUEST)
        .json({ message: "Bad Request: Id are required." })
      return;
    }

    const service: ServiceMonhoc = new ServiceMonhoc();
    service.monhoc_id = id
    const result: OnlyMonHoc = await service.get()

    if (result) {
      res.status(HttpStatus.OK).json(result)
    } else {
      res
        .status(HttpStatus.INTERNAL_SERVER_ERROR)
        .json({ message: "Internal Server Error: Unable to fetch data." })
    }
  } catch (error) {
    console.error("Error in ControllerLopHoc:", error)
    res
      .status(HttpStatus.INTERNAL_SERVER_ERROR)
      .json({ message: "Internal Server Error" })
  }
}

async function getAll(req: Request, res: Response): Promise<void> {
  try {
    const limit: number = parseInt(req.query.limit as string, 10)
    const offset: number = parseInt(req.query.offset as string, 10)

    if (isNaN(limit) || isNaN(offset)) {
      res
        .status(HttpStatus.BAD_REQUEST)
        .json({ message: "Bad Request: Both limit and offset are required." })
      return
    }

    const service = new ServiceMonhoc()
    const result = await service.getAll(limit, offset)

    if (result) {
      res.json(result)
    } else {
      res
        .status(HttpStatus.INTERNAL_SERVER_ERROR)
        .json({ message: "Internal Server Error: Unable to fetch data." })
    }
  } catch (error) {
    console.error("Error in ControllerLopHoc:", error)
    res
      .status(HttpStatus.INTERNAL_SERVER_ERROR)
      .json({ message: "Internal Server Error" })
  }
}
export default {
  insert,
  update,
  remove,
  get,
  getAll,
}
