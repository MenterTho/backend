import { Request, Response } from "express"
import HttpStatus from "http-status-codes"

import ServiceDeThi from "../services/dethi"

async function insert(req: Request, res: Response): Promise<void> {
  try {
    const dethimonhoc_id: number = req.body.dethimonhoc_id
    const sinhvien_id: number = req.body.sinhvien_id

    if (isNaN(dethimonhoc_id) || isNaN(sinhvien_id) || dethimonhoc_id < 1 || sinhvien_id < 1) {
      res.status(HttpStatus.BAD_REQUEST).json({ message: "Bad Request: dethimonhoc_id and sinhvien_id are required." })
      return
    }

    const service: ServiceDeThi = new ServiceDeThi()
    service.dethimonhoc_id = dethimonhoc_id
    service.sinhvien_id = sinhvien_id
    const result: boolean = await service.insert()

    if (result) {
      res.status(HttpStatus.OK).json({message: "Insert DeThi successfully!"})
    } else {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error: Unable to fetch data." })
    }
  } catch (error) {
    console.error("Error in ControllerDeThi:", error)
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error" })
  }
}

async function getAll(req: Request, res: Response): Promise<void> {
  try {
    const limit: number = parseInt(req.query.limit as string, 10)
    const offset: number = parseInt(req.query.offset as string, 10)

    if (isNaN(limit) || isNaN(offset)) {
      res.status(HttpStatus.BAD_REQUEST).json({ message: "Bad Request: Both limit and offset are required." })
      return
    }

    const service = new ServiceDeThi()
    const result = await service.getAll(limit, offset)

    if (result) {
      res.json(result)
    } else {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error: Unable to fetch data." })
    }
  } catch (error) {
    console.error("Error in ControllerDeThi:", error)
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error" })
  }
}

async function getAllOnSinhVien(req: Request, res: Response): Promise<void> {
  try {
    const sinhvien_id: number = parseInt(req.params.sinhvien_id, 10)
    const limit: number = parseInt(req.query.limit as string, 10)
    const offset: number = parseInt(req.query.offset as string, 10)

    if (isNaN(sinhvien_id) || isNaN(limit) || isNaN(offset) || sinhvien_id < 1) {
      res.status(HttpStatus.BAD_REQUEST).json({ message: "Bad Request: Both sinhvien_id, limit and offset are required." })
      return
    }

    const service = new ServiceDeThi()
    const result = await service.getAllOnSinhVien(sinhvien_id, limit, offset)

    if (result) {
      res.json(result)
    } else {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error: Unable to fetch data." })
    }
  } catch (error) {
    console.error("Error in ControllerDeThi:", error)
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error" })
  }
}

async function getAllOnTime(req: Request, res: Response): Promise<void> {
  try {
    const time: string = req.params.time
    const limit: number = parseInt(req.query.limit as string, 10)
    const offset: number = parseInt(req.query.offset as string, 10)

    if (!time || isNaN(limit) || isNaN(offset)) {
      res.status(HttpStatus.BAD_REQUEST).json({ message: "Bad Request: Both time, limit and offset are required." })
      return
    }

    const service = new ServiceDeThi()
    const result = await service.getAllOnTime(time, limit, offset)

    if (result) {
      res.json(result)
    } else {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error: Unable to fetch data." })
    }
  } catch (error) {
    console.error("Error in ControllerDeThi:", error)
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error" })
  }
}

export default { insert, getAll, getAllOnSinhVien, getAllOnTime }