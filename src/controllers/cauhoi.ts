import { Request, Response } from "express"
import HttpStatus from "http-status-codes"

import ServiceCauHoi from "../services/cauhoi"
import { LastOrCount } from "../models/alltypes"
import { ArrayCauHoi, OnlyCauHoi } from "../models/cauhoi"

async function insert(req: Request, res: Response): Promise<void> {
  try {
    const chuong_id: number = req.body.chuong_id
    const name: string = req.body.name

    if (isNaN(chuong_id) || chuong_id < 1) {
      res.status(HttpStatus.BAD_REQUEST).json({ message: "Bad Request: Id are required." })
      return
    }

    const service: ServiceCauHoi = new ServiceCauHoi()
    service.chuong_id = chuong_id
    service.cauhoi_name = name
    const result: boolean = await service.insert()

    if (result) {
      res.status(HttpStatus.OK).json({message: "Insert CauHoi successfully!"})
    } else {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error: Unable to fetch data." })
    }
  } catch (error) {
    console.error("Error in ControllerCauHoi:", error)
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error" })
  }
}

async function update(req: Request, res: Response): Promise<void> {
  try {
    const id: number = req.body.id
    const chuong_id: number = req.body.chuong_id
    const cautraloi_id: number = req.body.cautraloi_id
    const name: string = req.body.name

    if (isNaN(id) || isNaN(chuong_id) || isNaN(cautraloi_id) ||
        id < 1 || chuong_id < 1 || cautraloi_id < 1) {
      res.status(HttpStatus.BAD_REQUEST).json({ message: "Bad Request: Id is required and chuong_id, cautraloi_id are optional." })
      return
    }

    const service: ServiceCauHoi = new ServiceCauHoi()
    service.cauhoi_id = id
    service.chuong_id = chuong_id
    service.cautraloi_id = cautraloi_id
    service.cauhoi_name = name
    const result: boolean = await service.update()

    if (result) {
      res.status(HttpStatus.OK).json({message: "Update CauHoi successfully!"})
    } else {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error: Unable to fetch data." })
    }
  } catch (error) {
    console.error("Error in ControllerCauHoi:", error)
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error" })
  }
}

async function updateCautraloi(req: Request, res: Response): Promise<void> {
  try {
    const id: number = req.body.id
    const cautraloi_id: number = req.body.cautraloi_id

    if (isNaN(id) || isNaN(cautraloi_id) || id < 1 || cautraloi_id < 1) {
      res.status(HttpStatus.BAD_REQUEST).json({ message: "Bad Request: Id and cautraloi_id are required." })
      return
    }

    const service: ServiceCauHoi = new ServiceCauHoi()
    service.cauhoi_id = id
    service.cautraloi_id = cautraloi_id
    const result: boolean = await service.updateCautraloi()

    if (result) {
      res.status(HttpStatus.OK).json({message: "Update CauHoi successfully!"})
    } else {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error: Unable to fetch data." })
    }
  } catch (error) {
    console.error("Error in ControllerCauHoi:", error)
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error" })
  }
}

async function remove(req: Request, res: Response): Promise<void> {
  try {
    const id: number = parseInt(req.params.id as string, 10)

    if (isNaN(id) || id < 1) {
      res.status(HttpStatus.BAD_REQUEST).json({ message: "Bad Request: Id are required." })
      return
    }

    const service: ServiceCauHoi = new ServiceCauHoi()
    service.cauhoi_id = id
    const result: boolean = await service.remove()

    if (result) {
      res.status(HttpStatus.OK).json(result)
    } else {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error: Unable to fetch data." })
    }
  } catch (error) {
    console.error("Error in ControllerCauHoi:", error)
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error" })
  }
}

async function get(req: Request, res: Response): Promise<void> {
  try {
    const id: number = parseInt(req.params.id, 10)

    if (isNaN(id) || id < 1) {
      res.status(HttpStatus.BAD_REQUEST).json({ message: "Bad Request: Id are required." })
      return
    }

    const service: ServiceCauHoi = new ServiceCauHoi()
    service.cauhoi_id = id
    const result: OnlyCauHoi = await service.get()

    if (result) {
      res.status(HttpStatus.OK).json(result)
    } else {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error: Unable to fetch data." })
    }
  } catch (error) {
    console.error("Error in ControllerCauHoi:", error)
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error" })
  }
}

async function getAll(req: Request, res: Response): Promise<void> {
  try {
    const limit: number = parseInt(req.query.limit as string, 10)
    const offset: number = parseInt(req.query.offset as string, 10)

    // Kiểm tra dữ liệu đầu vàoif (isNaN(chuong_id) || isNaN(limit) 
    if (isNaN(limit) || isNaN(offset) || limit < 1 || offset < 0) {
      res.status(HttpStatus.BAD_REQUEST).json({ message: "Bad Request: Both limit and offset are required." })
      return
    }

    const service: ServiceCauHoi = new ServiceCauHoi()
    const result: ArrayCauHoi = await service.getAll(limit, offset)

    if (result) {
      res.status(HttpStatus.OK).json(result)
    } else {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error: Unable to fetch data." })
    }
  } catch (error) {
    console.error("Error in ControllerCauHoi:", error)
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error" })
  }
}

async function getAllOnChuong(req: Request, res: Response): Promise<void> {
  try {
    const chuong_id: number = parseInt(req.params.id, 10)
    const limit: number = parseInt(req.query.limit as string, 10)
    const offset: number = parseInt(req.query.offset as string, 10)

    if (isNaN(chuong_id) || isNaN(limit) || isNaN(offset) || chuong_id < 1 || limit < 1 || offset < 0) {
      res.status(HttpStatus.BAD_REQUEST).json({ message: "Bad Request: Both chuong_id params, limit and offset query are required." })
      return
    }

    const service: ServiceCauHoi = new ServiceCauHoi()
    service.chuong_id = chuong_id
    const result: ArrayCauHoi = await service.getAllOnChuong(limit, offset)

    if (result) {
      res.status(HttpStatus.OK).json(result)
    } else {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error: Unable to fetch data." })
    }
  } catch (error) {
    console.error("Error in ControllerCauHoi:", error)
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error" })
  }
}

async function getAllOnMonHoc(req: Request, res: Response): Promise<void> {
  try {
    const cauhoi_id: number = parseInt(req.params.id, 10)
    const limit: number = parseInt(req.query.limit as string, 10)
    const offset: number = parseInt(req.query.offset as string, 10)

    if (isNaN(cauhoi_id) || isNaN(limit) || isNaN(offset) || cauhoi_id < 1 || limit < 1 || offset < 0) {
      res.status(HttpStatus.BAD_REQUEST).json({ message: "Bad Request: Both chuong_id params, limit and offset query are required." })
      return
    }

    const service: ServiceCauHoi = new ServiceCauHoi()
    const result: ArrayCauHoi = await service.getAllOnMonHoc(cauhoi_id, limit, offset)

    if (result) {
      res.status(HttpStatus.OK).json(result)
    } else {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error: Unable to fetch data." })
    }
  } catch (error) {
    console.error("Error in ControllerCauHoi:", error)
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error" })
  }
}

async function getCount(res: Response): Promise<void> {
  try {
    const service: ServiceCauHoi = new ServiceCauHoi()
    const result: LastOrCount = await service.getCount()

    if (result) {
      res.status(HttpStatus.OK).json(result)
    } else {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error: Unable to fetch data." })
    }
  } catch (error) {
    console.error("Error in ControllerCauHoi:", error)
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error" })
  }
}

async function searchAll(req: Request, res: Response): Promise<void> {
  try {
    const keyword: string = req.params.keyword
    const limit: number = parseInt(req.query.limit as string)
    const offset: number = parseInt(req.query.offset as string)

    if (!keyword || isNaN(limit) || isNaN(offset) || limit < 1 || offset < 0) {
      res.status(HttpStatus.BAD_REQUEST).json({ message: "Bad Request: Both keyword, keyword, limit and offset are required." })
      return
    }

    const service: ServiceCauHoi = new ServiceCauHoi()
    const result: ArrayCauHoi = await service.searchAll(keyword, limit, offset)

    if (result) {
      res.status(HttpStatus.OK).json(result)
    } else {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error: Unable to fetch data." })
    }
  } catch (error) {
    console.error("Error in ControllerCauHoi:", error)
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error" })
  }
}

export default {
  insert, update, updateCautraloi, remove,
  get, getAll, getAllOnChuong, getAllOnMonHoc, getCount,
  searchAll
}