import { Request, Response } from "express"
import HttpStatus from "http-status-codes"

import ServiceCauTraLoi from "../services/cautraloi"
import { ArrayCauTraLoi, OnlyCauTraLoi } from "../models/cautraloi"

async function insert(req: Request, res: Response): Promise<void> {
  try {
    const id: number = req.body.id
    const cauhoi_id: number = req.body.cauhoi_id
    const name: string = req.body.name

    // Kiểm tra dữ liệu đầu vào
    if (isNaN(id) || id < 1 || isNaN(cauhoi_id) || cauhoi_id < 1) {
      res.status(HttpStatus.BAD_REQUEST).json({ message: "Bad Request: id, cauhoi_id are required." })
      return
    }

    const service: ServiceCauTraLoi = new ServiceCauTraLoi()
    service.cauhoi_id = id
    service.cautraloi_id = id
    service.cautraloi_name = name
    const result: boolean = await service.insert()

    if (result) {
      res.status(HttpStatus.OK).json({message: "Insert CauTraLoi successfully!"})
    } else {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error: Unable to fetch data." })
    }
  } catch (error) {
    console.error("Error in ControllerCauTraLoi:", error)
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error" })
  }
}

async function update(req: Request, res: Response): Promise<void> {
  try {
    const id: number = req.body.id
    const cauhoi_id: number = req.body.cauhoi_id
    const name: string = req.body.name

    // Kiểm tra dữ liệu đầu vào
    if (isNaN(id) || id < 1 || isNaN(cauhoi_id) || cauhoi_id < 1) {
      res.status(HttpStatus.BAD_REQUEST).json({ message: "Bad Request: id, cauhoi_id are required." })
      return
    }

    const service: ServiceCauTraLoi = new ServiceCauTraLoi()
    service.cauhoi_id = id
    service.cautraloi_id = id
    service.cautraloi_name = name
    const result: boolean = await service.update()

    if (result) {
      res.status(HttpStatus.OK).json({message: "Update CauTraLoi successfully!"})
    } else {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error: Unable to fetch data." })
    }
  } catch (error) {
    console.error("Error in ControllerCauTraLoi:", error)
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error" })
  }
}

async function remove(req: Request, res: Response): Promise<void> {
  try {
    const id: number = parseInt(req.params.id as string, 10)

    // Kiểm tra dữ liệu đầu vào
    if (isNaN(id) || id < 1) {
      res.status(HttpStatus.BAD_REQUEST).json({ message: "Bad Request: id, cauhoi_id are required." })
      return
    }

    const service: ServiceCauTraLoi = new ServiceCauTraLoi()
    service.cauhoi_id = id
    const result: boolean = await service.remove()

    if (result) {
      res.status(HttpStatus.OK).json({message: "Remove CauTraLoi successfully!"})
    } else {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error: Unable to fetch data." })
    }
  } catch (error) {
    console.error("Error in ControllerCauTraLoi:", error)
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error" })
  }
}

async function get(req: Request, res: Response): Promise<void> {
  try {
    const id: number = parseInt(req.params.id, 10)

    // Kiểm tra dữ liệu đầu vào
    if (isNaN(id) || id < 1) {
      res.status(HttpStatus.BAD_REQUEST).json({ message: "Bad Request: Id are required." })
      return
    }

    const service: ServiceCauTraLoi = new ServiceCauTraLoi()
    service.cautraloi_id = id
    const result: OnlyCauTraLoi = await service.get()

    if (result) {
      res.status(HttpStatus.OK).json(result)
    } else {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error: Unable to fetch data." })
    }
  } catch (error) {
    console.error("Error in ControllerCauTraLoi:", error)
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error" })
  }
}

async function getAll(req: Request, res: Response): Promise<void> {
  try {
    const limit: number = parseInt(req.query.limit as string, 10)
    const offset: number = parseInt(req.query.offset as string, 10)

    // Kiểm tra dữ liệu đầu vào
    if (isNaN(limit) || isNaN(offset) || limit < 1 || offset < 0) {
      res.status(HttpStatus.BAD_REQUEST).json({ message: "Bad Request: Both limit and offset are required." })
      return
    }

    const service: ServiceCauTraLoi = new ServiceCauTraLoi()
    const result: ArrayCauTraLoi = await service.getAll(limit, offset)

    if (result) {
      res.status(HttpStatus.OK).json(result)
    } else {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error: Unable to fetch data." })
    }
  } catch (error) {
    console.error("Error in ControllerCauTraLoi:", error)
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error" })
  }
}

async function getAllOnCauHoi(req: Request, res: Response): Promise<void> {
  try {
    const cauhoi_id: number = parseInt(req.params.cauhoi_id, 10)

    if (isNaN(cauhoi_id) || cauhoi_id < 1) {
      res.status(HttpStatus.BAD_REQUEST).json({ message: "Bad Request: Both cauhoi_id params, limit and offset query are required." })
      return
    }

    const service: ServiceCauTraLoi = new ServiceCauTraLoi()
    service.cauhoi_id = cauhoi_id
    const result: ArrayCauTraLoi = await service.getAllOnCauHoi()

    if (result) {
      res.status(HttpStatus.OK).json(result)
    } else {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error: Unable to fetch data." })
    }
  } catch (error) {
    console.error("Error in ControllerCauTraLoi:", error)
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error" })
  }
}

async function searchAll(req: Request, res: Response): Promise<void> {
  try {
    const keyword: string = req.params.keyword
    const limit: number = parseInt(req.query.limit as string)
    const offset: number = parseInt(req.query.offset as string)

    if (!keyword || isNaN(limit) || isNaN(offset) || limit < 1 || offset < 0) {
      res.status(HttpStatus.BAD_REQUEST).json({ message: "Bad Request: Both keyword, keyword, limit and offset are required." })
      return
    }

    const service: ServiceCauTraLoi = new ServiceCauTraLoi()
    const result: ArrayCauTraLoi = await service.searchAll(keyword, limit, offset)

    if (result) {
      res.status(HttpStatus.OK).json(result)
    } else {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error: Unable to fetch data." })
    }
  } catch (error) {
    console.error("Error in ControllerCauTraLoi:", error)
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error" })
  }
}

export default {
  insert, update, remove,
  get, getAll, getAllOnCauHoi,
  searchAll
}