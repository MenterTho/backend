import bcrypt from "bcrypt"
import { Request, Response } from "express"
import HttpStatus from "http-status-codes"

import ServiceNguoiDung from "../services/nguoidung"
import { ArrayNguoiDung, NguoiDungPassword } from "../models/nguoidung"

async function insert(req: Request, res: Response): Promise<void> {
  try {
    const nguoidung_account: string = req.body.account
    const quyen_id: number = parseInt(req.body.quyen_id, 10)
    const nguoidung_password: string = req.body.password
    const nguoidung_displayname: string = req.body.displayname

    if (!nguoidung_account ||
        !nguoidung_password ||
        isNaN(quyen_id) ||
        quyen_id < 1) {
      res.status(HttpStatus.BAD_REQUEST).json(
        { message: "Bad Request: Account, password and quyen_id are required."
      })
      return
    }

    const service: ServiceNguoiDung = new ServiceNguoiDung()
    service.nguoidung_account = nguoidung_account
    service.quyen_id = quyen_id
    service.nguoidung_password = await bcrypt.hash(nguoidung_password, 10)
    service.nguoidung_displayname = nguoidung_displayname
    const result: boolean = await service.insert()

    if (result) {
      res.status(HttpStatus.OK).json({message: "Insert NguoiDung successfully!"})
    } else {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error: Unable to fetch data." })
    }
  } catch (error) {
    console.error("Error in ControllerNguoiDung:", error)
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error" })
  }
}

// Chưa hoàn thành
async function update(req: Request, res: Response): Promise<void> {
  try {
    const nguoidung_account: string = req.body.account
    const quyen_id: number = parseInt(req.body.quyen_id, 10)
    const nguoidung_password: string = req.body.password
    const nguoidung_displayname: string = req.body.displayname

    if (!nguoidung_account || !nguoidung_password || isNaN(quyen_id) ||
        quyen_id < 1) {
      res.status(HttpStatus.BAD_REQUEST).json(
        { message: "Bad Request: Account, password and quyen_id are required."
      })
      return
    }

    // Kiểm tra password đưa vào có
    const service: ServiceNguoiDung = new ServiceNguoiDung()
    service.nguoidung_account = nguoidung_account
    const resultPassword: NguoiDungPassword = await service.getPassword()

    service.nguoidung_account = nguoidung_account
    service.quyen_id = quyen_id
    service.nguoidung_password = nguoidung_password
    service.nguoidung_displayname = nguoidung_displayname
    const result: boolean = await service.update()

    if (result) {
      res.status(HttpStatus.OK).json({message: "Update NguoiDung successfully!"})
    } else {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error: Unable to fetch data." })
    }
  } catch (error) {
    console.error("Error in ControllerNguoiDung:", error)
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error" })
  }
}

async function remove(req: Request, res: Response): Promise<void> {
  try {
    const nguoidung_account: string = req.body.account

    if (!nguoidung_account) {
      res.status(HttpStatus.BAD_REQUEST).json(
        { message: "Bad Request: Account is required."
      })
      return
    }

    const service: ServiceNguoiDung = new ServiceNguoiDung()
    service.nguoidung_account = nguoidung_account
    const result: boolean = await service.remove()

    if (result) {
      res.status(HttpStatus.OK).json({message: "Remove NguoiDung successfully!"})
    } else {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error: Unable to fetch data." })
    }
  } catch (error) {
    console.error("Error in ControllerNguoiDung:", error)
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error" })
  }
}

async function getAll(req: Request, res: Response): Promise<void> {
  try {
    const limit: number = parseInt(req.query.limit as string, 10)
    const offset: number = parseInt(req.query.offset as string, 10)

    // Kiểm tra dữ liệu đầu vào
    if (isNaN(limit) || isNaN(offset) || limit < 1 || offset < 0) {
      res.status(HttpStatus.BAD_REQUEST).json({ message: "Bad Request: Both limit and offset are required." })
      return
    }

    const service: ServiceNguoiDung = new ServiceNguoiDung()
    const result: ArrayNguoiDung = await service.getAll(limit, offset)

    if (result) {
      res.status(HttpStatus.OK).json(result)
    } else {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error: Unable to fetch data." })
    }
  } catch (error) {
    console.error("Error in ControllerNguoiDung:", error)
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error" })
  }
}

export default {
  insert, update, remove, getAll
}