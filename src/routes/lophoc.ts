import express from "express"
import Controller from "../controllers/lophoc"

const router = express.Router()

router.post("/insert", Controller.insert)
router.put("/update", Controller.update)
router.delete("/remove/:id", Controller.remove)
router.get("/:id", Controller.get)
router.get("/", Controller.getAll)
router.get("/countmonhoc/:id", Controller.getCountMonhoc)
router.get("/countsinhvien/:id", Controller.getCountSinhvien)

export default router