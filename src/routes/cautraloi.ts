import express from "express"
import Controller from "../controllers/cautraloi"

const router = express.Router()

router.post("/insert", Controller.insert)
router.put("/update", Controller.update)
router.delete("/remove", Controller.remove)
router.get("/", Controller.getAll)
router.get("/:id", Controller.get)
router.get("/cauhoi/:cauhoi_id", Controller.getAllOnCauHoi)
router.get("/search/:keyword", Controller.searchAll)

export default router