import express from "express"
import Controller from "../controllers/dethi"

const router = express.Router()

router.post("/")
router.get("/", Controller.getAll)
router.get("/sinhvien/:id", Controller.getAllOnSinhVien)

export default router