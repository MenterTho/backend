import express from "express"
import Controller from "../controllers/chuong"

const router = express.Router()
router.get("/", Controller.getAll)
router.post("/insert", Controller.insert)
router.get("/:id", Controller.get)
router.delete("/remove", Controller.remove)
router.put("/update", Controller.update)
router.get("/count", Controller.getCount)
router.get("/monhoc/:id", Controller.getAllChuongOnMonHoc)
export default router
