import express from "express"
import Controller from "../controllers/dethimonhoc"

const router = express.Router()

router.post("/insert", Controller.insert)
router.put("/update", Controller.update)
router.delete("/remove/:id", Controller.remove)
router.get("/", Controller.getAll)
router.get("/monhoc/:monhoc_id", Controller.getAllOnMonhoc)
router.get("/count", Controller.getCount)

export default router