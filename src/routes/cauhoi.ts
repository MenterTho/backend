import express from "express"
import Controller from "../controllers/cauhoi"

const router = express.Router()

router.post("/insert", Controller.insert)
router.put("/update", Controller.update)
router.put("/update/cautraloi", Controller.updateCautraloi)
router.delete("/remove/:id", Controller.remove)
router.get("/:id", Controller.get)
router.get("/", Controller.getAll)
router.get("/chuong/:id", Controller.getAllOnChuong)
router.get("/monhoc/:id", Controller.getAllOnMonHoc)
router.get("/count", Controller.getCount)
router.get("/search/:keyword", Controller.searchAll)

export default router