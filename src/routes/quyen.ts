import express from "express"
import Controller from "../controllers/quyen"

const router = express.Router()

router.post("/insert", Controller.insert)
router.put("/update", Controller.update)
router.delete("/remove", Controller.remove)
router.get("/", Controller.getAll)

export default router