import express from "express"
import Controller from "../controllers/cauhoidethi"

const router = express.Router()

router.put("/update/cautraloi", Controller.updateCautraloi)
router.get("/", Controller.getAll)
router.get("/dethi/:dethi_id", Controller.getAllOnDethi)

export default router