import express from "express"
import Controller from "../controllers/monhoc"

const router = express.Router()
router.get("/", Controller.getAll)
router.post("/insert", Controller.insert)
router.get("/:id", Controller.get)
router.delete("/remove", Controller.remove)
router.put("/update", Controller.update)

export default router
