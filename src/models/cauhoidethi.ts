export type CauHoiDeThi = {
  cauhoidethi_id: number
  dethi_id: number
  cauhoi_id: number
  cautraloi_id: number
}

export type ArrayCauHoiDeThi = CauHoiDeThi[] | undefined