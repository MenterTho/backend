export type NguoiDung = {
  nguoidung_account: string
  quyen_id: number
  nguoidung_password: string
  nguoidung_displayname: string
}
export type NguoiDungPassword = string | undefined

export type ArrayNguoiDung = NguoiDung[] | undefined