export type Quyen = {
  quyen_id: number
  quyen_name: string
}

export type ArrayQuyen = Quyen[] | undefined