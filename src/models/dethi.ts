export type DeThi = {
  dethi_id: number
  dethimonhoc_id: string
  sinhvien_id: number
  dethi_startdate: string
  dethi_enddate: string
}

export type ArrayDeThi = DeThi[] | undefined