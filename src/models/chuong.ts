export type Chuong = {
  chuong_id: number
  monhoc_id: number
  chuong_number: number
  chuong_name: string
}
export type OnlyChuong = Chuong | undefined
export type ArrayChuong = Chuong[] | undefined
