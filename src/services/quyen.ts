import { QueryResult } from "pg"

import { ArrayQuyen, Quyen } from "../models/quyen"
import pool from "../config/database"

class ServiceQuyen {
  quyen_id!: number
  quyen_name!: string

  /* Các method cập nhập dữ liệu */
  async insert(): Promise<boolean> {
    try {
      await pool.query({
        text: "CALL insert_quyen($1);",
        values: [this.quyen_name]
      })

    } catch (error) {
      console.log(error)
      return false
    }

    return true
  }

  async update(): Promise<boolean> {
    try {
      if (this.quyen_id !== undefined)
        await pool.query({
          text: "CALL update_quyen($1, $2);",
          values: [this.quyen_id, this.quyen_name]
        })
    } catch (error) {
      console.log(error)
      return false
    }

    return true
  }

  async remove(): Promise<boolean> {
    try {
      await pool.query({
        text: "CALL delete_quyen($1);",
        values: [this.quyen_id]
      })

    } catch (error) {
      console.log(error)
      return false
    }

    return true
  }

  /* Các method lấy dữ liệu */
  async getAll(limit: number, offset: number): Promise<ArrayQuyen> {
    let allValues: ArrayQuyen = []

    try {
      // Execute SQL
      const result: QueryResult = await pool.query({
        text: "SELECT * FROM getall_quyen_withlimitoffset($1, $2);",
        values: [limit, offset],
      })

      // Convert result row to CauHoi and add to allValues
      allValues = result.rows.map((row: Quyen) => ({
        quyen_id: row.quyen_id,
        quyen_name: row.quyen_name
      }))
    } catch (error) {
      // Return undefined
      console.log(error)
      return undefined
    }

    return allValues
  }
}

export default ServiceQuyen