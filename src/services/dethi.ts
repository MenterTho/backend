import { QueryResult } from "pg"

import pool from "../config/database"
import { ArrayDeThi, DeThi } from "../models/dethi"

class ServiceDeThi {
  dethi_id!: number
  dethimonhoc_id!: number
  sinhvien_id!: number
  dethi_startdate!: string
  dethi_enddate!: string

  /* Các method cập nhập dữ liệu */
  async insert(): Promise<boolean> {
    try {
      await pool.query({
        text: "CALL insert_dethi($1, $2);",
        values: [this.dethimonhoc_id, this.sinhvien_id]
      })

    } catch (error) {
      console.log(error)
      return false
    }

    return true
  }

  async getAll(limit: number, offset: number):
    Promise<ArrayDeThi>
  {
    let allValues: ArrayDeThi = []

    try {
      // Execute SQL
      const result: QueryResult = await pool.query({
        text: "SELECT * FROM getall_dethi_withlimitoffset($1, $2);",
        values: [limit, offset],
      })

      // Convert result row to DeThi
      allValues = result.rows.map((row: DeThi) => ({
        dethi_id: row.dethi_id,
        dethimonhoc_id: row.dethimonhoc_id,
        sinhvien_id: row.sinhvien_id,
        dethi_startdate: row.dethi_startdate,
        dethi_enddate: row.dethi_enddate
      }))
    } catch (error) {
      // Return undefined
      console.log(error)
      return undefined
    }

    return allValues
  }

  async getAllOnMonHoc(id: number, limit: number, offset: number):
    Promise<ArrayDeThi>
  {
    let allValues: ArrayDeThi = []

    try {
      // Execute SQL
      const result: QueryResult = await pool.query({
        text: "SELECT * FROM getall_dethi_on_monhoc($1, $2, $3);",
        values: [id, limit, offset],
      })

      // Convert result row to DeThi
      allValues = result.rows.map((row: DeThi) => ({
        dethi_id: row.dethi_id,
        dethimonhoc_id: row.dethimonhoc_id,
        sinhvien_id: row.sinhvien_id,
        dethi_startdate: row.dethi_startdate,
        dethi_enddate: row.dethi_enddate
      }))
    } catch (error) {
      // Return undefined
      console.log(error)
      return undefined
    }

    return allValues
  }

  async getAllOnSinhVien(id: number, limit: number, offset: number):
    Promise<ArrayDeThi>
  {
    let allValues: ArrayDeThi = []

    try {
      // Execute SQL
      const result: QueryResult = await pool.query({
        text: "SELECT * FROM getall_dethi_on_sinhvien($1, $2, $3);",
        values: [id, limit, offset],
      })

      // Convert result row to DeThi
      allValues = result.rows.map((row: DeThi) => ({
        dethi_id: row.dethi_id,
        dethimonhoc_id: row.dethimonhoc_id,
        sinhvien_id: row.sinhvien_id,
        dethi_startdate: row.dethi_startdate,
        dethi_enddate: row.dethi_enddate
      }))
    } catch (error) {
      // Return undefined
      console.log(error)
      return undefined
    }

    return allValues
  }

  async getAllOnTime(time: string, limit: number, offset: number):
    Promise<ArrayDeThi>
  {
    let allValues: ArrayDeThi = []

    try {
      // Execute SQL
      const result: QueryResult = await pool.query({
        text: "SELECT * FROM getall_dethi_on_time($1, $2, $3);",
        values: [time, limit, offset],
      })

      // Convert result row to DeThi
      allValues = result.rows.map((row: DeThi) => ({
        dethi_id: row.dethi_id,
        dethimonhoc_id: row.dethimonhoc_id,
        sinhvien_id: row.sinhvien_id,
        dethi_startdate: row.dethi_startdate,
        dethi_enddate: row.dethi_enddate
      }))
    } catch (error) {
      // Return undefined
      console.log(error)
      return undefined
    }

    return allValues
  }
}

export default ServiceDeThi