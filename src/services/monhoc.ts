import { QueryResult } from "pg"

import pool from "../config/database"
import { ArrayMonHoc, MonHoc, OnlyMonHoc } from "../models/monhoc"
import { LastOrCount } from "../models/alltypes"
class ServiceMonhoc {
  monhoc_id!: number
  lophoc_id!: number
  monhoc_name!: string
  /* Các method cập nhập dữ liệu */
  async insert(): Promise<boolean> {
    try {
      await pool.query({
        text: "CALL insert_lophoc($1);",
        values: [this.monhoc_name],
      })
    } catch (error) {
      console.log(error)
      return false
    }

    return true
  }
  async update(): Promise<boolean> {
    try {
      await pool.query({
        text: "CALL update_lophoc($1, $2 , $3);",
        values: [this.monhoc_id, this.lophoc_id, this.monhoc_name],
      })
    } catch (error) {
      console.log(error)
      return false
    }

    return true
  }
  async remove(): Promise<boolean> {
    try {
      await pool.query({
        text: "CALL delete_monhoc($1);",
        values: [this.monhoc_id],
      })
    } catch (error) {
      console.log(error)
      return false
    }

    return true
  }
  // lay tung du lieu co trong mon hoc
  async get(): Promise<OnlyMonHoc> {
    let value: OnlyMonHoc

    try {
      // Execute SQL
      const result: QueryResult = await pool.query({
        text: "SELECT * FROM get_monhoc_withid($1);",
        values: [this.monhoc_id],
      })

      // result row to value
      value = result.rows[0]
    } catch (error) {
      // Return undefined
      console.log(error)
      return undefined
    }

    return value
  }
  // lay cac du lieu co trong chuong
  async getAll(limit: number, offset: number): Promise<ArrayMonHoc> {
    let allValues: ArrayMonHoc = []

    try {
      // Execute SQL
      const result: QueryResult = await pool.query({
        text: "SELECT * FROM getall_monhoc_withlimitoffset($1, $2)",
        values: [limit, offset],
      })

      // Convert result row to CauHoi and add to allValues
      allValues = result.rows.map((row: MonHoc) => ({
        monhoc_id: row.monhoc_id,
        lophoc_id: row.lophoc_id,
        monhoc_name: row.monhoc_name,
      }))
    } catch (error) {
      // Return undefined
      console.log(error)
      return undefined
    }

    return allValues
  }
}

export default ServiceMonhoc
