import { QueryResult } from "pg"

import pool from "../config/database"
import { ArrayCauHoi, CauHoi, OnlyCauHoi } from "../models/cauhoi"
import { LastOrCount } from "../models/alltypes"

class ServiceCauHoi {
  cauhoi_id!: number
  chuong_id!: number
  cautraloi_id!: number
  cauhoi_name!: string

  /* Các method cập nhập dữ liệu */
  async insert(): Promise<boolean> {
    try {
      await pool.query({
        text: "CALL insert_cauhoi($1, $2);",
        values: [this.chuong_id, this.cauhoi_name]
      })

    } catch (error) {
      console.log(error)
      return false
    }

    return true
  }

  // Chưa hoàn thành
  async update(): Promise<boolean> {
    try {
      if (this.chuong_id !== undefined)
        await pool.query({
          text: "CALL update_cauhoi($1, $2, $3, $4);",
          values: [this.cauhoi_id, this.chuong_id, this.cautraloi_id, this.cauhoi_name]
        })
    } catch (error) {
      console.log(error)
      return false
    }

    return true
  }

  async updateCautraloi(): Promise<boolean> {
    try {
      if (this.chuong_id !== undefined)
        await pool.query({
          text: "CALL update_cauhoi_cautraloi($1, $2);",
          values: [this.cauhoi_id, this.cautraloi_id]
        })
    } catch (error) {
      console.log(error)
      return false
    }

    return true
  }

  async remove(): Promise<boolean> {
    try {
      await pool.query({
        text: "CALL delete_cauhoi($1);",
        values: [this.cauhoi_id]
      })

    } catch (error) {
      console.log(error)
      return false
    }

    return true
  }

  /* Các method lấy dữ liệu */
  async get(): Promise<OnlyCauHoi>
  {
    let value: OnlyCauHoi

    try {
      // Execute SQL
      const result: QueryResult = await pool.query({
        text: "SELECT * FROM get_cauhoi_withid($1);",
        values: [this.cauhoi_id],
      })

      // result row to value
      value = result.rows[0]
    } catch (error) {
      // Return undefined
      console.log(error)
      return undefined
    }

    return value
  }

  async getAll(limit: number, offset: number): Promise<ArrayCauHoi>
  {
    let allValues: ArrayCauHoi = []

    try {
      // Execute SQL
      const result: QueryResult = await pool.query({
        text: "SELECT * FROM getall_cauhoi_withlimitoffset($1, $2);",
        values: [limit, offset],
      })

      // Convert result row to CauHoi and add to allValues
      allValues = result.rows.map((row: CauHoi) => ({
        cauhoi_id: row.cauhoi_id,
        chuong_id: row.chuong_id,
        cautraloi_id: row.cautraloi_id,
        cauhoi_name: row.cauhoi_name,
      }))
    } catch (error) {
      // Return undefined
      console.log(error)
      return undefined
    }

    return allValues
  }

  async getAllOnChuong(limit: number, offset: number):
    Promise<ArrayCauHoi>
  {
    let allValues: ArrayCauHoi = []

    try {
      // Execute SQL
      const result: QueryResult = await pool.query({
        text: "SELECT * FROM getall_cauhoi_on_chuong($1, $2, $3);",
        values: [this.chuong_id, limit, offset],
      })

      // Convert result row to CauHoi and add to allValues
      allValues = result.rows.map((row: CauHoi) => ({
        cauhoi_id: row.cauhoi_id,
        chuong_id: row.chuong_id,
        cautraloi_id: row.cautraloi_id,
        cauhoi_name: row.cauhoi_name,
      }))
    } catch (error) {
      // Return undefined
      console.log(error)
      return undefined
    }

    return allValues
  }

  async getAllOnMonHoc(monhoc: number, limit: number, offset: number):
    Promise<ArrayCauHoi>
  {
    let allValues: ArrayCauHoi = []

    try {
      // Execute SQL
      const result: QueryResult = await pool.query({
        text: "SELECT * FROM getall_cauhoi_on_monhoc($1, $2, $3);",
        values: [monhoc, limit, offset],
      })

      // Convert result row to CauHoi and add to allValues
      allValues = result.rows.map((row: CauHoi) => ({
        cauhoi_id: row.cauhoi_id,
        chuong_id: row.chuong_id,
        cautraloi_id: row.cautraloi_id,
        cauhoi_name: row.cauhoi_name,
      }))
    } catch (error) {
      // Return undefined
      console.log(error)
      return undefined
    }

    return allValues
  }

  async getCount(): Promise<LastOrCount> {
    let value: LastOrCount

    try {
      // Execute SQL
      const result: QueryResult = await pool.query({
        text: "SELECT * FROM getcount_cauhoi();"
      })

      // result row to value
      value = result.rows[0]
    } catch (error) {
      // Return undefined
      console.log(error)
      return undefined
    }

    return value
  }

  async searchAll(keyword: string, limit: number, offset: number): 
    Promise<ArrayCauHoi>
  {
    let allValues: ArrayCauHoi = []

    try {
      // Execute SQL
      const result: QueryResult = await pool.query({
        text: "SELECT * FROM searchall_cauhoi($1, $2, $3);",
        values: [keyword as string, limit, offset],
      })

      // Convert result row to CauHoi and add to allValues
      allValues = result.rows.map((row: CauHoi) => ({
        cauhoi_id: row.cauhoi_id,
        chuong_id: row.chuong_id,
        cautraloi_id: row.cautraloi_id,
        cauhoi_name: row.cauhoi_name,
      }))
    } catch (error) {
      // Return undefined
      console.log(error)
      return undefined
    }

    return allValues
  }
}

export default ServiceCauHoi