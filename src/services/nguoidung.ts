import { QueryResult } from "pg"

import pool from "../config/database"
import { ArrayNguoiDung, NguoiDung, NguoiDungPassword } from "../models/nguoidung"

class ServiceNguoiDung {
  nguoidung_account!: string
  quyen_id!: number
  nguoidung_password!: string
  nguoidung_displayname!: string

  /* Các method cập nhập dữ liệu */
  async insert(): Promise<boolean> {
    try {
      await pool.query({
        text: "CALL insert_nguoidung($1, $2, $3, $4);",
        values: [
          this.nguoidung_account,
          this.quyen_id,
          this.nguoidung_password,
          this.nguoidung_displayname
        ]
      })

    } catch (error) {
      console.log(error)
      return false
    }

    return true
  }

  // Chưa hoàn thành
  async update(): Promise<boolean> {
    try {
      if (this.nguoidung_account !== undefined)
        await pool.query({
          text: "CALL update_nguoidung($1, $2, $3, $4);",
          values: [
            this.nguoidung_account,
            this.quyen_id,
            this.nguoidung_password,
            this.nguoidung_displayname
          ]
        })
    } catch (error) {
      console.log(error)
      return false
    }

    return true
  }

  async remove(): Promise<boolean> {
    try {
      await pool.query({
        text: "CALL delete_nguoidung($1);",
        values: [this.nguoidung_account]
      })

    } catch (error) {
      console.log(error)
      return false
    }

    return true
  }

  async getAll(limit: number, offset: number): Promise<ArrayNguoiDung>
  {
    let allValues: ArrayNguoiDung = []

    try {
      // Execute SQL
      const result: QueryResult = await pool.query({
        text: "SELECT * FROM getall_cauhoi_withlimitoffset($1, $2);",
        values: [limit, offset],
      })

      // Convert result row to CauHoi and add to allValues
      allValues = result.rows.map((row: NguoiDung) => ({
        nguoidung_account: row.nguoidung_account,
        quyen_id: row.quyen_id,
        nguoidung_password: row.nguoidung_password,
        nguoidung_displayname: row.nguoidung_displayname,
      }))
    } catch (error) {
      // Return undefined
      console.log(error)
      return undefined
    }

    return allValues
  }

  async getPassword(): Promise<NguoiDungPassword> {
    let value: NguoiDungPassword

    try {
      // Execute SQL
      const result: QueryResult = await pool.query({
        text: "SELECT * FROM get_nguoidung_password($1);",
        values: [this.nguoidung_account],
      })

      // result row to value
      value = result.rows[0]
    } catch (error) {
      // Return undefined
      console.log(error)
      return undefined
    }

    return value
  }
}

export default ServiceNguoiDung