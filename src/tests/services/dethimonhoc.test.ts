 import { LastOrCount } from "../../models/alltypes"
import { ArrayDeThiMonHoc } from "../../models/dethimonhoc"
import ServiceDeThiMonHoc from "../../services/dethimonhoc"

test("Nhập dữ liệu bảng dethimonhoc", async () => {
  let sv: ServiceDeThiMonHoc = new ServiceDeThiMonHoc()
  sv.monhoc_id = 1
  sv.dethimonhoc_name = "Lịch sử 45p"
  sv.dethimonhoc_real = true
  sv.dethimonhoc_questions = 20
  sv.dethimonhoc_time = 45
  const result: boolean = await sv.insert()
  expect(result).not.toBe(false)
})

test("Cập nhập dữ liệu bảng dethimonhoc", async () => {
  let sv: ServiceDeThiMonHoc = new ServiceDeThiMonHoc()
  sv.dethimonhoc_id = 4
  sv.monhoc_id = 1
  sv.dethimonhoc_name = "Lịch sử 45p, kiểm tra chính thức"
  sv.dethimonhoc_real = true
  sv.dethimonhoc_questions = 20
  sv.dethimonhoc_time = 45
  const result: boolean = await sv.update()
  expect(result).not.toBe(false)
})

test("Xóa dữ liệu bảng dethimonhoc", async () => {
  let sv: ServiceDeThiMonHoc = new ServiceDeThiMonHoc()
  sv.dethimonhoc_id = 4
  const result: boolean = await sv.remove()
  expect(result).not.toBe(false)
})

test("Trả về các bảng dethimonhoc với limit và offset", async () => {
  let sv: ServiceDeThiMonHoc = new ServiceDeThiMonHoc()
  const result: ArrayDeThiMonHoc = await sv.getAll(10, 0)
  expect(result).not.toBe(undefined)
})

test("Trả về các bảng dethimonhoc với monhoc_id, limit và offset", async () => {
  let sv: ServiceDeThiMonHoc = new ServiceDeThiMonHoc()
  sv.monhoc_id = 1
  const result: ArrayDeThiMonHoc = await sv.getAllOnMonhoc(10, 0)
  expect(result).not.toBe(undefined)
})

test("Trả về tổng giá trị bảng dethimonhoc", async () => {
  let sv: ServiceDeThiMonHoc = new ServiceDeThiMonHoc()
  const result: LastOrCount = await sv.getCount()
  expect(result).not.toBe(undefined)
})