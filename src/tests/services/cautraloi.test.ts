import ServiceCauTraLoi from "../../services/cautraloi"

test("Trả về bảng CauTraLoi dựa trên id", async () => {
  let sv: ServiceCauTraLoi = new ServiceCauTraLoi()
  sv.cautraloi_id = 1
  const result = await sv.get()
  expect(result).not.toBe(undefined)
})
test("Trả về hết bảng CauTraLoi dựa trên limit và offset", async () => {
  let sv: ServiceCauTraLoi = new ServiceCauTraLoi()
  const result = await sv.getAll(10, 0)
  expect(result).not.toBe(undefined)
})
test("Trả về hết bảng CauTraLoi phụ vào CauHoi", async () => {
  let sv: ServiceCauTraLoi = new ServiceCauTraLoi()
  sv.cauhoi_id = 1
  const result = await sv.getAllOnCauHoi()
  expect(result).not.toBe(undefined)
})
test("Trả về hết bảng CauTraLoi phụ vào tìm kiếm dựa trên limit và offset", async () => {
  let sv: ServiceCauTraLoi = new ServiceCauTraLoi()
  const result = await sv.searchAll("1", 10, 0)
  expect(result).not.toBe(undefined)
})